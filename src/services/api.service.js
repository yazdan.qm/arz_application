// imports
import axios from "axios";
import router from "../router";
import { useBaseStore } from "../stores/base";
import { push } from "notivue";

// headers
const headers = {
  accept: `application/json`,
  "Content-Type": `application/json`,
};

// api instance
const api = axios.create({
  headers: headers,
});

// error handling
const error_handler = (err) => {
  console.log(err);
  if (err.response.status === 403) {
    push.error(err.response.data.data.message_detail)
  } else {
    router.push({ name: "Error" });
  }
};

export { api, error_handler };
