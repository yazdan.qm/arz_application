export default [
  {
    path: "/",
    component : () => import("@/layout/MasterLayout.vue"),
    children: [
      {
        path: "/",
        name: "Home",
        component: () => import("@/views/HomePage.vue"),
      },
      {
        path: "/contact",
        name: "Contact",
        component: () => import("@/views/ContactPage.vue"),
      },
      {
        path: "/about",
        name: "About",
        component: () => import("@/views/AboutUsPage.vue"),
      },
      {
        path: "/branches",
        name: "Branches",
        component: () => import("@/views/BranchesPage.vue"),
      },
      {
        path: "/login",
        name: "Login",
        component: () => import("@/views/LoginView.vue"),
      },
    ],
  },
  {
    path: "/error",
    name: "Error",
    component: () => import("@/views/ErrorPage.vue"),
  },
];
