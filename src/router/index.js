import { createRouter, createWebHistory } from '@ionic/vue-router';
import general from "@/router/paths"

const routes = [
    ...general
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
