import { api, error_handler } from "@/services/api.service";
import { push } from "notivue";
import { defineStore } from "pinia";
import { ref } from "vue";

export const useBaseStore = defineStore("base", () => {
  // data
  const csrf_token = ref(null);
  const is_user_logged_in = ref(false);
  const user = ref(null);
  const loading = ref(null);

  // methods
  const getUserData = (JWTToken) => {
    loading.value = true;
    const formData = new FormData();
    formData.append("jwt", JWTToken);
    api
      .post("https://www.ayandehexchange.com/API/jwt_decode", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        user.value = res.data.data.payload;
        is_user_logged_in.value = true;
        push.success(
          "Welcome dear" +
            " " +
            user.value?.customer_first_name +
            " " +
            user.value?.customer_last_name
        );
      })
      .catch((err) => {
        error_handler(err);
      })
      .finally(() => {
        loading.value = false;
      });
  };

  return { csrf_token, user, is_user_logged_in, loading, getUserData };
});
