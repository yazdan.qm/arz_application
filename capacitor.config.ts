import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'arz.application',
  appName: 'arz-application',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
