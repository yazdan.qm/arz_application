/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontWeight: {
        thin: "100",
        ultralight: "200",
        light: "300",
        medium: "400",
        semibold: "500",
        bold: "600",
        extrabold: "700",
        black: "800",
        heavy: "900",
      },
      colors: {
        bg: {
          primary: "#f6f7ff",
          secondary: "#ffffff",
        },
        txt: {
          primary: "#364b60",
          secondary: "#9aa4b0",
          blue: "#3ea0bf",
          low: "#b1b1b1",
          red: "#e74c3c",
          green: "#2ecc71",
        },
      },
    },
  },
  plugins: [],
};
